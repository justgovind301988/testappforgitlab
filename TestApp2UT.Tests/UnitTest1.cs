using TestApp2;
using Xunit;

namespace TestApp2UT.Tests
{

    public class UnitTest1
    {
        [Fact]
        public void TestMethod1()
        {
            Class1 c = new Class1();
            string actual = c.Concat("Hello", "Test");
            Assert.Equal("Hello Test", actual);
        }
    }
}
