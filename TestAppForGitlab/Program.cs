﻿using System;
using TestApp2;

namespace TestAppForGitlab
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Math m = new Math();
            int total = m.Add(2, 3);
            Console.WriteLine(total);

            Class1 c = new Class1();
            string output = c.Concat("Hello", "Tripstack");
            Console.WriteLine(output);
            //Console.ReadLine();
        }
    }
}
