﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestAppForGitlab
{
    public class Math
    {
        public int Add(int a, int b)
        {
            if (a == 0 || b == 0)
            {
                return 0;
            }
            else
                return a + b;
        }
    }
}
