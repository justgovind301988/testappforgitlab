using Xunit;

namespace TestAppForGitlab.Tests
{

    public class UnitTest1
    {
        [Fact]
        public void TestMethod1()
        {
            Math m = new Math();
            int totalActual = m.Add(1, 2);
            Assert.Equal(3, totalActual);
        }
        [Fact]
        public void TestMethod2()
        {
            Math m = new Math();
            int totalActual = m.Add(2, 5);
            Assert.Equal(7, totalActual);
        }
        [Fact]
        public void TestMethod3()
        {
            Program.Main(null);
        }
    }
}
