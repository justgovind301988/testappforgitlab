﻿using System;

namespace TestApp2
{
    public class Class1
    {
        public string Concat(string a, string b)
        {
            return string.Format($"{a} {b}");
        }
    }
}
